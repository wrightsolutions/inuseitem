#!/usr/bin/env python
# ss command hint by giving argument of zero
# python ./inusetcp.py 0
#
# Current limitation is that in order to do the numeric sort it will
# fold same port entries into a single entry

import subprocess
import re
import shlex
from sys import exit as sysexit
from sys import argv as sysargv
from os import sep as ossep
from os import path as ospath
from os import stat as osstat

COLON=chr(58)
PARENTH_OPEN=chr(40)
PARENTH_CLOSE=chr(41)
DQ=chr(34)
SQ=chr(39)
NSTAT_RPM_BASED='/sbin/nstat'

CMD_T_TEMPLATE="""{0}/ss -l -p -n -t -4 -6"""
CMD_U_TEMPLATE="""{0}/ss -l -p -n -u -4 -6"""
SED2="""/bin/sed 's/[ ]\{2,\}/\\t/g' """
SED2="""/bin/sed 's/zZz/999/g' """
#CMD_T_TEMPLATE="""{0}/ss -l -p -n -t -4 -6 | {1}"""
#CMD_U_TEMPLATE="""{0}/ss -l -p -n -u -4 -6 | {1}"""

RE_NUMERIC_MARKER = re.compile('\d')

# Next define a regex we can use to remove lines that look like a header
RE_ADDRESS_PORT = re.compile('dress{0}Port'.format(COLON))

RE_SIXLIKE = re.compile('{0}{0}'.format(COLON))
RE_USERS_PID = re.compile(r"users{0}\{1}\{1}{2}.*{2},pid=".format(COLON,PARENTH_OPEN,DQ))
RE_USERS_CSV = re.compile(r"users{0}{1}{1}<>{2}{2}".format(COLON,PARENTH_OPEN,PARENTH_CLOSE))


def isfile_and_positive_size(target_path_given):
    target_path = target_path_given.strip()
    if not ospath.isfile(target_path):
        return False
    if not osstat(target_path).st_size:
        return False
    return True


if isfile_and_positive_size(NSTAT_RPM_BASED):
	CMD_T = CMD_T_TEMPLATE.format('/sbin')
	CMD_U = CMD_U_TEMPLATE.format('/sbin')
else:
	# Debian and derivatives
	CMD_T = CMD_T_TEMPLATE.format('/bin')
	CMD_U = CMD_U_TEMPLATE.format('/bin')
#print(CMD_T)


def keylist_sorted(keys):
	kdict = {}
	for k in keys:
		try:
			k_array = k.split(COLON)
			key = k_array[-1]
		except:
			key = k

		kdict[key] = k

	#print(kdict)
	klist = kdict.keys()
	# Next an 'in place' numeric sort
	klist.sort(key=int)
	#print(klist)
	# Now things are in numeric order we return to the real keys 
	keys_sorted = [ kdict[key] for key in klist ]

	return keys_sorted


def port_dicts(output):
	port4dict = {}
	port6dict = {}
	for line in output:
		line_array = None
		if RE_ADDRESS_PORT.search(line):
			line_array = None
			continue
		elif RE_NUMERIC_MARKER.search(line):
			line_array = line.split()
		else:
			pass

		if line_array is None:
			continue

		#print(line)
		if RE_SIXLIKE.search(line):
			# Has :: so probably an ipv6 line
			address_port = line_array[3]
			port6dict[address_port]=line
			#print "# Unable to append line {0}".format(line)
		else:
			address_port = line_array[3]
			port4dict[address_port]=line
			#print(line_array)

	return (port4dict,port6dict)


if __name__ == '__main__':

	program_binary = sysargv[0].strip()

	port=None
	if len(sysargv) > 1:
		port_string = sysargv[1].strip()
		try:
			port=int(port_string)
		except:
			sysexit(101)

	if 0==port:
		print("ss -l -p -n -t -4 -6 | /bin/sed 's/[ ]\{2,\}/\\t/g'")
		sysexit(100)

	program_binary_basename = ospath.basename(program_binary)

	"""
	cmd_split = shlex.split(CMD_T)
	print(cmd_split)
	subout = (subprocess.Popen(cmd_split,bufsize=4096, \
		stdout=subprocess.PIPE,stderr=subprocess.STDOUT)).stdout
	"""
	pss = subprocess.Popen(shlex.split(CMD_T),bufsize=4096, \
		stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
	pgrep = subprocess.Popen(shlex.split(SED2),bufsize=4096, \
		stdin=pss.stdout,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
	#pgrepout, pgreperr = pgrep.communicate(pss.stdout)
	pgrepout, pgreperr = pgrep.communicate()
	pss.stdout.close()  # Allow pss to receive a SIGPIPE if pgrep exits.
	#pgrep.stdout.close()  # Allow pgrep to receive a SIGPIPE

	plines = pgrepout.split('\n')
	port4dict,port6dict = port_dicts(plines)

	keys4 = keylist_sorted(port4dict.iterkeys())
	#print(keys4)
	keys6 = keylist_sorted(port6dict.iterkeys())

	for key in keys4:
		print(port4dict[key])
	for key in keys6:
		print(port6dict[key])
